close all
clear

% Plot flops (in G) projections for CIS and NTO variants for wf overlap

% Read occupied and virtual orbitals values
file = "test_systems.csv";
input = dlmread(file, ',', 1,4);

% Occupied orbitals
occ = input(:,2);

% Virtual orbitals
vir = input(:,3);

% Number of states to simulate
n_states = 100;
s = 1:1:n_states;

% Function that computes the CIS variant Gflops
% no - number of occupied orbitals
% nv - number of virtual orbitals
% ns - number of states
function flops = cis_flops (no, nv, ns)
  
  %flops = (0.5 * no^7 + no^2 * nv^2 * (4*no^2 + no + 4 * ns.^2))/1024^3;
  c = 0.25*no^7 + 2.0*no^4*nv + 2.0*no^3*nv^2;
  a1 = 2.0*no*nv^2;
  a2 = no^2*nv;
  flops = (c + a1*ns + a2*ns.^2)/1024^3;
  
endfunction

% Function that computes the NTO variant Gflops
% no - number of occupied orbitals
% ns - number of states
function flops = nto_flops (no, ns)
  
  a = 4.0 * no^5;
  flops = (a * ns.^2)/1024^3;
  
endfunction

step = 4;  % Should be even number

for j = 1:step:size(occ,1)
  
  % Put 4 graphs in new figure
  figure;
  hold on;

  for i = j:j+step-1
    indx = i-j+1
    flops_cis = cis_flops(occ(i), vir(i), s);
    flops_nto = nto_flops(occ(i), s);

    subplot(step/2, 2, indx)
    plot(s, flops_cis, 'r', 'linewidth', 3, flops_nto, 'b', 'linewidth', 3);
  
    title_text = ['CIS/NTO flops count. no = ', num2str(occ(i)), ', nv = ', num2str(vir(i))];
    title(title_text, "fontsize", 12);
    xlabel("Number of states", "fontsize", 12);
    ylabel("Complexity (giga-ops)", "fontsize", 12);
    h = legend("CIS", "NTO", "location", "northwest");
    set(h, "fontsize", 12)
  endfor
endfor

