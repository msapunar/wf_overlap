#!/bin/bash

# Script submits the "submit_test-minors" script for testing different versions of computing all determinants of the l2-minors
# The script is submitted to ONLY one node, therefor, only the number of cpus (threads) are required to be set.
# The script is based on the SLURM workload manager. For other workload managers or when testing on local machine, some modifications
# in the scripts are required.
# The main computational part is done in the script: 
#
# The input parameters are:
#
# $1 - start matrix dimension
# $2 - end matrix dimensioin
# $3 - differentce between two matrix dimensions (a step size in the loop from start to end matrix dimension)
# $4 - number of cpus per task
#
# ================================================================================================================================
 
#
# EXECUTABLE PART
#

# Print help
if [ "$1" == "-h"  ]; then
	usage="$(basename "$0") [-h] [nmin nmax step ncpus] -- program to test the algorithm for computing all the determinants of the given matrix for different matrix sizes. The script submits jobs via SLURM workload manager

	where:
		nmin	start matrix dimension
		nmax	end matrix dimension
		step	matrix dimension step
		ncpus	number of cores to use
		-h	show this help text"
	echo "$usage"
	exit 1
fi

# SLURM parameters 
jobname="test_wf_overlap_${4}"
outfile="large_"
errfile="large_"
nodes=1
ntasks=1
cpus_per_task=$4
partition="batch"
 
# SCRIPT parameters
exec="run.test-minors"
nmin=$1
nmax=$2
step=$3
infile="large_no/cscmat"

sbatch --job-name=${jobname} -D . --output=${outfile}%j.out --error=${errfile}%j.err --nodes=${nodes} --ntasks=${ntasks} --cpus-per-task=${cpus_per_task} --time=10:00:00 --partition=${partition} --exclusive ${exec} ${nmin} ${nmax} ${step} ${infile}
