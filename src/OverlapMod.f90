!--------------------------------------------------------------------------------------------------
! MODULE: CISOverlapMod
!
! DESCRIPTION: 
!--------------------------------------------------------------------------------------------------
module cisoverlapmod
    ! Import variables
    use constants
    ! Import subroutines
    use mathmod
    !$ use omp_lib

    implicit none
    
    private
    public :: cis_olap_full


contains


    subroutine cis_olap_full(csc, no, nv1, nv2, wf1, wf2, omat)
        real(dp), intent(in) :: csc(:, :, :) ! MO overlap matrix.
        integer, intent(in) :: no(:) !< Number of occupied orbitals.
        integer, intent(in) :: nv1(:) !< Number of virtual orbitals (bra).
        integer, intent(in) :: nv2(:) !< Number of virtual orbitals (ket).
        type(rmat), intent(inout) :: wf1(:) !< Ket wave function coefficients.
        type(rmat), intent(inout) :: wf2(:) !< Bra wave function coefficients.
        real(dp), allocatable, intent(out) :: omat(:, :) !< Overlap matrix.
        real(dp), allocatable :: rr(:)
        real(dp), allocatable :: rs(:, :)
        real(dp), allocatable :: sr(:, :)
        real(dp), allocatable :: ss(:, :, :)
        integer :: rhf, rhf1, rhf2, ns1, ns2, s1, s2, s
        integer :: st1, st2

        ! Timing variables
	real(dp) :: start, finish
	real(dp) :: starttot, finishtot, total
	real(dp) :: wfbt, rrat, rsblkt, srblkt, ssblkt, omatt 

        ! Initialization
        rhf = size(csc, 3) 
        rhf1 = size(wf1)
        rhf2 = size(wf2)
        ns1 = size(wf1(1)%c, 2)
        ns2 = size(wf2(1)%c, 2)
        allocate(rr(rhf))
        allocate(rs(ns2, rhf))
        allocate(sr(ns1, rhf))
        allocate(ss(ns1, ns2, rhf))
        if (.not. allocated(omat)) allocate(omat(ns1 + 1, ns2 + 1))

!	call cpu_time(starttot)
        starttot = omp_get_wtime()

!	call cpu_time(start)
        start = omp_get_wtime()
        if (rhf == 2) then
            ! Renormalize wave functions if they have different number of spin functions:
            if (rhf1 == 1) wf1(1)%c = wf1(1)%c / sqrt(2.0_dp)
            if (rhf2 == 1) wf2(1)%c = wf2(1)%c / sqrt(2.0_dp)
        end if
!	call cpu_time(finish)
        finish = omp_get_wtime()
        wfbt = finish - start

        print '("COMPUTE BLOCK START")'
        do s = 1, rhf
            s1 = min(s, rhf1) 
            s2 = min(s, rhf2)

            ! Calculate blocks:
            print '("Computing rrblock...")'
            start = omp_get_wtime()
            call rrblock(csc(:, :, s), no(s), rr(s))
            finish = omp_get_wtime()
            rrat = finish - start

            print '("Computing rsblock...")'
            start = omp_get_wtime()
            call rsblock(csc(:, :, s), no(s), nv2(s2), ns2, wf2(s2)%c, rs(:, s))
            finish = omp_get_wtime()
            rsblkt = finish - start
    
            print '("Computing srblock...")'
            start = omp_get_wtime()
            call srblock(csc(:, :, s), no(s), nv1(s1), ns1, wf1(s1)%c, sr(:, s))
            finish = omp_get_wtime()
            srblkt = finish - start
    
            print '("Computing ssblock...")'
            start = omp_get_wtime()
            call ssblock(csc(:, :, s), no(s), nv1(s1), nv2(s2), ns1, ns2, wf1(s1)%c, wf2(s2)%c,    &
            &            ss(:, :, s))
            finish = omp_get_wtime()
            ssblkt = finish - start 

        end do
        print '("COMPUTE BLOCK END")'

        start = omp_get_wtime()
        if (rhf == 1) then
            ! Ground state overlap:
            omat(1, 1) = rr(1) * rr(1)
            ! Ground-excited state overlap:
            do st1 = 1, ns1
                omat(st1 + 1, 1) = rr(1) * sr(st1, 1)
            end do
            ! Excited-ground state overlap:
            do st2 = 1, ns2
                omat(1, st2 + 1) = rr(1) * rs(st2, 1)
            end do
            ! Excited-excited state overlap:
            do st1 = 1, ns1 
                do st2 = 1, ns2 
                    omat(st1 + 1, st2 + 1) = rr(1) * ss(st1, st2, 1) + sr(st1, 1) * rs(st2, 1)
                end do
            end do
        else
            ! Ground state overlap:
            omat(1, 1) = rr(1) * rr(2)
            ! Ground-excited state overlap:
            do st1 = 1, ns1
                omat(st1 + 1, 1) = rr(1) * sr(st1, 2) + rr(2) * sr(st1, 1)
            end do
            ! Excited-ground state overlap:
            do st2 = 1, ns2
                omat(1, st2 + 1) = rr(2) * rs(st2, 1) + rr(1) * rs(st2, 2)
            end do
            ! Excited-excited state overlap:
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1 + 1, st2 + 1) = rr(2) * ss(st1, st2, 1) + sr(st1, 1) * rs(st2, 2)     &
                    &                      + rr(1) * ss(st1, st2, 2) + sr(st1, 2) * rs(st2, 1)
                end do
            end do
        end if
        finish = omp_get_wtime()
        omatt = finish - start

        finishtot = omp_get_wtime()
        total = finishtot - starttot

        ! Print timings
        print '("Execution time:")'
        write(*,101) 'alpha & beta: ', wfbt, 'sec'
        write(*,101) 'rra: ', rrat, 'sec'
        write(*,101) 'rsblock: ', rsblkt,'sec'
        write(*,101) 'srblock: ', rsblkt, 'sec'
        write(*,101) 'ssblock: ', ssblkt, 'sec'
        write(*,101) 'omat: ', omatt, 'sec'
        print '("")'
        write(*,102) 'Total execution time: ', total, 'sec'
        print '("")'

        101 FORMAT(A14, G12.5, A4)
        102 FORMAT(A22, G12.5, A4)

    end subroutine cis_olap_full
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RRBlock
    !> @brief Reference determinant overlap.
    !----------------------------------------------------------------------------------------------
    subroutine rrblock(csc, no, rr)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        real(dp), intent(out) :: rr !< Determinant.
        rr = det(csc(1:no, 1:no))
    end subroutine rrblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SRBlock
    !> @brief Overlaps of singly excited bra determinants with ket reference determinant.
    !----------------------------------------------------------------------------------------------
    subroutine srblock(csc, no, nv1, ns1, wf1, sr)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv1 !< Number of bra virtual orbitals.
        integer, intent(in) :: ns1 !< Number of bra states.
        real(dp), intent(in) :: wf1(no*nv1, ns1) !< Bra wf coefficients.
        real(dp), intent(out) :: sr(ns1) 

        integer :: o
        integer :: v
        integer :: st
        real(dp) :: cdet
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: tmp(:, :)
        integer :: num_threads

        ! External procedures defined in LAPACK
#ifdef OPENBLAS
        external openblas_set_num_threads !$pragma c(openblas_set_num_threads)
        integer, external :: openblas_get_num_threads
#elif MKL
        external mkl_set_num_threads
        integer, external :: mkl_get_max_threads
#endif
        allocate(ref(1:no, 1:no))
        allocate(tmp(1:no, 1:no))

        ref = csc(1:no, 1:no)
        sr = 0.0_dp

        ! Store the current number of openblas threads
        ! OpenMP paralelization is used and single-threaded BLAS
#ifdef OPENBLAS
        num_threads = openblas_get_num_threads()
        call openblas_set_num_threads(1)
#elif MKL
        num_threads = mkl_get_max_threads()
        call mkl_set_num_threads(1)
#endif
        !$omp parallel shared (sr)
        !$omp do private(tmp, cdet) schedule(dynamic) reduction(+:sr)
        do o = 1, no
            tmp = ref
            do v = 1, nv1
                tmp(o, :) = csc(no + v, 1:no)
                cdet = det(tmp)
                do st = 1, ns1
                    sr(st) = sr(st) + cdet * wf1((o-1)*nv1 + v, st)
                end do
            end do
        end do
        !$omp end do
        !$omp end parallel

#ifdef OPENBLAS
        ! Restore to the original number of openblas threads
        call openblas_set_num_threads(num_threads)
#elif MKL
        call mkl_set_num_threads(num_threads)
#endif

    end subroutine srblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RSBlock
    !> @brief Overlaps of bra reference determinant with singly excited ket determinants.
    !----------------------------------------------------------------------------------------------
    subroutine rsblock(csc, no, nv2, ns2, wf2, rs)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv2 !< Number of ket virtual orbitals.
        integer, intent(in) :: ns2 !< Number of ket states.
        real(dp), intent(in) :: wf2(no*nv2, ns2) !< Ket wf coefficients.
        real(dp), intent(out) :: rs(ns2)

        integer :: o
        integer :: v
        integer :: st
        real(dp) :: cdet
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: tmp(:, :)
        integer :: num_threads

#ifdef OPENBLAS
        ! External procedures defined in LAPACK
        external openblas_set_num_threads !$pragma c(openblas_set_num_threads)
        integer, external :: openblas_get_num_threads
#elif MKL
        integer, external :: mkl_get_max_threads
#endif
        allocate(ref(1:no, 1:no))
        allocate(tmp(1:no, 1:no))
        ref = csc(1:no, 1:no)
        rs = 0.0_dp

#ifdef OPENBLAS
        ! Store the current number of openblas threads
        ! OpenMP paralelization is used and single-threaded BLAS
        num_threads = openblas_get_num_threads()
        call openblas_set_num_threads(1)
#elif MKL
        num_threads = mkl_get_max_threads()
        call mkl_set_num_threads(1)
#endif
        !$omp parallel shared(rs)
        !$omp do private(tmp, cdet) schedule(dynamic) reduction(+:rs)
        do o = 1, no
            tmp = ref
            do v = 1, nv2
                tmp(:, o) = csc(1:no, no + v)
                cdet = det(tmp)
                do st = 1, ns2
                    rs(st) = rs(st) + cdet * wf2((o-1)*nv2 + v, st)
                end do
            end do
        end do
        !$omp end do
        !$omp end parallel
#ifdef OPENBLAS
        ! Restore to the original number of openblas threads
        call openblas_set_num_threads(num_threads)
#elif MKL
        call mkl_set_num_threads(num_threads)
#endif

    end subroutine rsblock

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SSBlock
    !> @brief Overlaps of singly excited bra and ket determinants.
    !----------------------------------------------------------------------------------------------
    subroutine ssblock(csc, no, nv1, nv2, ns1, ns2, wf1, wf2, ss)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv1 !< Number of bra virtual orbitals.
        integer, intent(in) :: nv2 !< Number of ket virtual orbitals.
        integer, intent(in) :: ns1 !< Number of bra states.
        integer, intent(in) :: ns2 !< Number of ket states.
        real(dp), intent(in) :: wf1(no*nv1, ns1) !< Bra wf coefficients.
        real(dp), intent(in) :: wf2(no*nv2, ns2) !< Ket wf coefficients.
        real(dp), intent(out) :: ss(ns1, ns2)

        integer :: o1
        integer :: o2
        integer :: v1
        integer :: v2
        integer :: st1
        integer :: st2
        integer :: pos1, pos2, pos3, pos
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: tmp(:, :)
        real(dp), allocatable :: tmp2(:, :)
#if LINALG
	real(dp), allocatable :: cdet(:)
	real(dp), allocatable :: wfProd(:)
        integer :: num_threads
        integer :: wfLDA
#else
        real(dp) :: cdet
#endif

        ! Timing variables
	real(dp) :: start, finish
	real(dp) :: cdet_time, ss_time 
	real(dp) :: wfprod_time, ger_time, ddot_time, startt

#ifdef LINALG
        ! External procedures defined in LAPACK
	real(dp), external :: ddot
        external dger, dscal, dgemm
#ifdef OPENBLAS
        external openblas_set_num_threads !$pragma c(openblas_set_num_threads)
        integer, external :: openblas_get_num_threads
#elif MKL
        external mkl_set_num_threads
        integer, external :: mkl_get_max_threads
#endif
#endif

        allocate(ref(1:no, 1:no))
        allocate(tmp(1:no, 1:no))
        allocate(tmp2(1:no, 1:no))
#ifdef LINALG
        allocate(cdet(no*no*nv1*nv2))
        wfLDA = no*no*nv1*nv2
        allocate(wfProd(wfLDA))
#endif
        
        ss = 0.0_dp
        ref = csc(1:no, 1:no)

#ifdef LINALG
        ! Store the current number of openblas threads
        ! OpenMP paralelization is used with single-threaded BLAS
#ifdef OPENBLAS
        num_threads = openblas_get_num_threads()
        call openblas_set_num_threads(1)
#elif MKL
        num_threads = mkl_get_max_threads()
        call mkl_set_num_threads(1)
#endif
        call omp_set_num_threads(num_threads)
        write(*,*) 'Number of threads for OpenMP loops set to: ', num_threads
        start = omp_get_wtime()

        !$omp parallel default(shared)
        !$omp do private(tmp, tmp2, pos, v1, o2, v2) schedule(dynamic)
        do o1 = 1, no
          tmp = ref
          do v1 = 1, nv1
            tmp(o1, :) = csc(no + v1, 1:no)
            do o2 = 1, no
              tmp2 = tmp
              do v2 = 1, nv2
                tmp2(:, o2) = csc(1:no, no + v2)
                tmp2(o1, o2) = csc(no + v1, no + v2)
                pos = (o1-1)*nv1*nv2*no + (v1-1)*nv2*no + (o2-1)*nv2 + v2
                cdet(pos) = det(tmp2)
              end do
            end do
          end do
        end do
        !$omp end do
        !$omp end parallel

        finish = omp_get_wtime()
        cdet_time = finish -start


        ! Now using parallel BLAS calls. No OpenMP paralelization
#ifdef OPENBLAS
        call openblas_set_num_threads(num_threads)
#elif MKL
        call mkl_set_num_threads(num_threads)
#endif

        ger_time = 0.0d0
        ddot_time = 0.0d0
        start = omp_get_wtime()

        !!$omp parallel default(shared) 
        !!$omp do private(st2, wfProd) schedule(dynamic)
        do st1 = 1, ns1
          do st2 = 1, ns2
            startt = omp_get_wtime()
            wfProd = 0.0_dp
            wfprod_time = wfprod_time + (omp_get_wtime()-startt)

            ! Compute wf2(st2) * wf1(st1) results in a matrix of dimension (no*nv2) x (no*nv1)
            startt = omp_get_wtime()
            call dger(no*nv2, no*nv1, 1.0d0, wf2(1,st2), 1, wf1(1,st1), 1, wfProd(1), no*nv2)
            ger_time = ger_time + (omp_get_wtime()-startt)

           ! Scalar product of cdet (all computed determinants) with a combination of wf2/wf1
           startt = omp_get_wtime()
           ss(st1, st2) = ss(st1, st2) + ddot(no*no*nv1*nv2, cdet(1), 1, wfProd(1), 1)
           ddot_time = ddot_time + (omp_get_wtime()-startt)
          end do
        end do
        !!$omp end do
        !!$omp end parallel

        finish = omp_get_wtime()
        ss_time = finish - start

        write(*,105) "Construct cdet:", cdet_time, "sec"
        write(*,105) "Generate ss:", ss_time, "sec"
        write(*,105) "set wfprod 0:", wfprod_time, "sec"
        write(*,105) "dger:", ger_time, "sec"
        write(*,105) "ddot:", ddot_time, "sec"

        105 FORMAT(A15, G12.5, A4)
#else
        !$omp parallel shared(ss)
        !$omp do private(tmp, tmp2, cdet) schedule(dynamic) reduction(+:ss)
        do o1 = 1, no
          !print '("[SSBLOCK] Outer loop pass: ",I0)', o1
          tmp = ref
          do v1 = 1, nv1
            tmp(o1, :) = csc(no + v1, 1:no)
            do o2 = 1, no
              tmp2 = tmp
              do v2 = 1, nv2
                tmp2(:, o2) = csc(1:no, no + v2)
                tmp2(o1, o2) = csc(no + v1, no + v2)
                cdet = det(tmp2)
                !write(*,*) o1, o2, v1, v2, cdet
                !write(*,*) cdet
                do st1 = 1, ns1
                  do st2 = 1, ns2
                    ss(st1, st2) = ss(st1, st2) + cdet * wf1((o1-1)*nv1 + v1, st1) &
                                                    &  * wf2((o2-1)*nv2 + v2, st2)
                    !write(*,*) wf1((o1-1)*nv1+v1,st1), wf2((o2-1)*nv2 + v2, st2)
                  end do
                end do
              end do
            end do
          end do
        end do
        !$omp end do
        !$omp end parallel
#endif	
    end subroutine ssblock

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: printMatrix
    !----------------------------------------------------------------------------------------------
    subroutine printMatrix(n, mat)
            integer, intent(in) :: n
	    real(dp), intent(in) :: mat(n,n)

            integer :: i, j

            do i = 1, n
               write(*,'(10000(e22.15,2x))') mat(i,:)
            end do
    end subroutine printMatrix

end module cisoverlapmod
