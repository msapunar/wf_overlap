program wfoverlap

    use constants
    use cisoverlapmod
    use ciswftype
    implicit none
    

    integer :: rhf !< Number of spin functions.
    integer :: n1 !< Total number of orbitals for bra wfs.
    integer :: n2 !< Total number of orbitals for ket wfs.
    real(dp), allocatable :: csc(:, :, :) !< Orbital overlap matrix for each spin.
                                          !! Dimensions :: n1, n2, rhf
    integer :: ns1 !< Number of bra states.
    integer :: ns2 !< Number of ket states.
    integer :: rhf1 !< Number of bra spin functions.
    integer :: rhf2 !< Number of ket spin functions.
    integer, allocatable :: no1(:) !< Number of occupied orbitals of each spin for bra wfs.
                                   !! Dimensions :: rhf1
    integer, allocatable :: no2(:) !< Number of occupied orbitals of each spin for ket wfs.
                                   !! Dimensions :: rhf2
    integer, allocatable :: nv1(:) !< Number of virtual orbitals of each spin for bra wfs.
                                   !! Dimensions :: rhf1
    integer, allocatable :: nv2(:) !< Number of virtual orbitals of each spin for ket wfs.
                                   !! Dimensions :: rhf2
    type(rmat), allocatable :: wf1(:) !< Bra wave function coefficients.
                                      !! Dimensions: rhf1
                                      !! Dimensions of wf1(s)%c: no(s)*nv1(s), ns1
    type(rmat), allocatable :: wf2(:) !< Ket wave function coefficients.
                                      !! Dimensions: rhf2
                                      !! Dimensions of wf2(s)%c: no(s)*nv2(s), ns2
    real(dp), allocatable :: en1(:) !< Bra wf energies.
    real(dp), allocatable :: en2(:) !< Ket wf energies.
    real(dp), allocatable :: omat(:, :) !< Final overlap matirx.
                                        !! Dimensions: ns1 + 1, ns2 + 1

    integer :: i
    integer :: st
    integer :: inunit
    integer :: iargc

    character(len=500) :: outfile
    integer, parameter :: out_unit = 20
    logical :: print_file = .FALSE.

    if (iargc() >= 1) then
	print_file = .TRUE.
	call getarg(1, outfile)
        write(*,*) 'The output will be written to file: ', outfile
    end if

    open(newunit=inunit, file='cscmat')
    read(inunit, *) rhf, n1, n2 ! Restricted/unrestricted, # orbitals wf1, # orbitals wf2.
    allocate(csc(n1, n2, rhf))
    !write(*,*) 'CSC dimension: ', n1, ' x ', n2
    do st = 1, rhf
        do i = 1, n2
            read(inunit, *) csc(:, i, st)
        end do
    end do
    close(inunit)

    call ciswfread_array('wf1', rhf1, no1, nv1, ns1, wf1, en1)
    call ciswfread_array('wf2', rhf2, no2, nv2, ns2, wf2, en2)

    if (rhf /= max(rhf1, rhf2)) then
        write(stderr, '(x,a)') 'Error reading input.'
        write(stderr, '(3x,3(a,i0))') 'Spin functions: CSC: ', rhf, ', WF1: ', rhf1, ', WF2:', rhf2
        stop
    end if
    do i = 1, rhf1
        if (no1(i) + nv1(i) /= n1) then
            write(stderr, '(x,a)') 'Error reading input.'
            write(stderr, '(3x,a,i0)') 'CSC matrix number of orbitals: ', n1
            write(stderr, '(3x,2(a,i0))') 'WF 1 number of spin ', i, ' orbitals: ',              &
            &                                no1(i) + nv1(i)
            stop
        end if
    end do
    do i = 1, rhf2
        if (no2(i) + nv2(i) /= n2) then
            write(stderr, '(x,a)') 'Error reading input.'
            write(stderr, '(3x,a,i0)') 'CSC matrix number of orbitals: ', n2
            write(stderr, '(3x,a,i0,a,i0)') 'WF 2 number of spin ', i, ' orbitals: ',              &
            &                                no2(i) + nv2(i)
            stop
        end if
    end do

    write(*,100) 'Orbitals bra wfs: ', n1
    write(*,100) 'Orbitals ket wfs: ', n2
    write(*,100) 'Num bra states:   ', ns1
    write(*,100) 'Num ket states:   ', ns2
    write(*,100) 'Spin functions:   ', rhf
    write(*,100) 'Num bra spins:   ', rhf
    write(*,100) 'Num ket spins:   ', rhf
    do i = 1, rhf1
       write(*,101) 'Bra spin func.:', i
       write(*,101) 'occu orbitals bra:', no1(i)
       write(*,101) 'virt orbitals bra:', nv1(i)
    end do
    do i = 1, rhf2
       write(*,101) 'Ket spin func.:', i
       write(*,101) 'occu orbitals ket:', no2(i)
       write(*,101) 'virt orbitals ket:', nv2(i)
    end do
    write(*,*)

    100 FORMAT(x,a18,i5) 
    101 FORMAT(4x,a18,i5)

    call cis_olap_full(csc, no1, nv1, nv2, wf1, wf2, omat)

    ! Open file to print output
    if (print_file) then
	    open(unit=out_unit, file=outfile, action="write", status='replace')
    end if
    do st = 1, ns1 + 1
        !write(*, '(10000(e22.15,2x))') omat(st, :)
	if (print_file) then
		write(out_unit, '(10000(e22.15,2x))') omat(st, :)
	end if
    end do
    if (print_file) then
	    close(out_unit)
    end if

end program wfoverlap
