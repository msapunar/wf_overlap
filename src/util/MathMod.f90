MODULE mathmod
    use constants
    IMPLICIT NONE


CONTAINS

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: det
    !> @brief Calculate determinant of a matrix.
    !> @details
    !! The determinant is calculated without changing the input matrix. The LAPACK subroutine for
    !! LU decomposition is used if LINALG is defined, otherwise the det_simple subroutine is used.
    !----------------------------------------------------------------------------------------------
    function det(a) result(adet)
        real(dp), intent(in) :: a(:, :)
        real(dp) :: adet
        integer :: n
        real(dp) :: atmp(size(a,1), size(a,2))

        n = size(a, 1)
        atmp = a

#ifdef LINALG
        adet = det_lapack(atmp, n)
#else
        adet = det_simple(atmp, n)
#endif
    end function det


#if LINALG
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: det_lapack
    !> @brief Use LAPACK LU decomposition to compute matrix determinant.
    !----------------------------------------------------------------------------------------------
    function det_lapack(a, n) result(adet)
        integer, intent(in) :: n !< Matrix dimensions.
        real(dp), intent(inout) :: a(n, n) !< Matrix. (On exit it is in LU form.)
        real(dp) :: adet
        integer :: ipiv(n)   ! pivot indices
        integer :: i
        integer :: sgn
        integer :: info

        ! External procedures defined in LAPACK
        external DGETRF

        ! DGETRF computes an LU factorization of a general M-by-N matrix A
        ! using partial pivoting with row interchanges.
        call DGETRF(n, n, a, n, ipiv, info)
        if (info /= 0) then
            adet = 0.0_dp
            return
        end if

        ! Get number of the pivot pairs, if the number of pairs is odd, then sgn is -1
        sgn = 1
        adet = 1.0_dp
        do i = 1, n
            if (ipiv(i) /= i) sgn = -sgn
            adet = adet * a(i, i)
        end do
        adet = sgn * adet
    end function det_lapack
#endif


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: det_simple
    !> @brief Calculate determinant of a matrix.
    !----------------------------------------------------------------------------------------------
    function det_simple(a, n) result(adet)
        integer, intent(in) :: n
        real(dp), intent(inout) :: a(n, n)
        real(dp) :: adet
        integer :: i, j, k, imax
        integer :: indx(n)
        real(dp) :: mat_max, dum, tsum
        real(dp) :: vv(n)
 
        adet = 1.0_dp
        do i = 1, n
            mat_max = 0.0_dp
            do j = 1, n
                if (abs(a(i, j)) > mat_max) mat_max = abs(a(i, j))
            end do
            if (mat_max < tinydp) then
                adet = 0.0_dp
                return
            end if
            vv(i) = 1.0_dp / mat_max
        end do
        do j = 1, n
            do i = 1, j-1
                tsum = a(i,j)
                do k = 1, i-1
                    tsum = tsum - a(i, k)*a(k, j)
                end do
                a(i,j) = tsum
            end do
            mat_max = 0.0_dp
            do i = j, n
                tsum = a(i,j)
                do k = 1, j-1
                    tsum = tsum - a(i, k)*a(k, j)
                end do
                a(i, j) = tsum
                dum = vv(i) * abs(tsum)
                if (dum >= mat_max) then
                    imax = i
                    mat_max = dum
                end if
            end do
            if (j /= imax) then
                do k = 1, n
                    dum = a(imax,k)
                    a(imax, k) = a(j, k)
                    a(j, k) = dum
                end do
                adet = -adet
                vv(imax) = vv(j)
            end if
            indx(j) = imax
            if (abs(a(j, j)) < tinydp) a(j, j) = tinydp
            if (j /= n) then
                dum = 1.0_dp / a(j, j)
                do i = j+1, n
                    a(i,j) = a(i,j) * dum
                end do
            end if
        end do
        do i = 1, n
            adet = adet * a(i, i)
        end do
    end function det_simple


end module mathmod
