module ciswftype
    use constants
    use stringmod
    implicit none

    private
    public :: ciswf
    public :: ciswfread
    public :: ciswfread_array
    public :: ciswfwrite

    !----------------------------------------------------------------------------------------------
    ! TYPE: CISWF
    !
    ! DESCRIPTION:
    !> @brief Contains all information about a set of CIS type wave functions.
    !----------------------------------------------------------------------------------------------
    type ciswf
        integer :: n = 0 !< Number of states.
        integer :: rhf = 0 !< Number of spin functions.
        integer, allocatable :: no(:) !< Number of occupied orbitals for each spin.
                                      !! Dimensions: rhf.
        integer, allocatable :: nv(:) !< Number of virtual orbitals for each spin.
                                      !! Dimensions: rhf.
        real(dp), allocatable :: e(:) !< Energy of each wave function.
                                      !! Dimensions: n.
        type(rmat), allocatable :: c(:, :) !< Coefficient matrices.
                                           !! Dimensions: rhf x n.
    end type ciswf


contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: CISWFWrite
    !
    ! DESCRIPTION:
    !> @brief Write a CIS type wave function.
    !> @details
    !! Write a CIS type wave function to a file. The format of the file is:
    !! @verbatim
    !! First line: Number of states and number of spin functions.
    !! For each spin:
    !!     Number of occupied and virtual orbitals of that spin.
    !! For each state:
    !!     First line: Energy of the state.
    !!     For each spin:
    !!         For each occupied orbital:
    !!             Excitation coefficients from the occupied orbital to each virtual orbital.
    !! End of file.
    !! @endverbatim
    !----------------------------------------------------------------------------------------------
    subroutine ciswfwrite(outfile, wf)
        character(len=*), intent(in) :: outfile !< Output file.
        type(ciswf), intent(in) :: wf !< Wave function.
        integer :: outunit !< Unit.
        integer :: st !< State iterator.
        integer :: s !< Spin iterator.
        integer :: o !< Occupied orbital iterator.

        open(newunit=outunit, file=outfile)
        write(outunit, 1000) wf%n, wf%rhf
        do s = 1, wf%rhf
            write(outunit, 1000) wf%no(s), wf%nv(s)
        end do
        do st = 1, wf%n
            write(outunit, 1001) wf%e(st)
            do s = 1, wf%rhf
                do o = 1, wf%no(s)
                    write(outunit, 1001) wf%c(s, st)%c(o, :)
                end do
            end do
        end do
        close(outunit)
 
        1000 format (2(x,i0))
        1001 format (10000(x,e23.17))
    end subroutine ciswfwrite


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: CISWFRead
    !
    ! DESCRIPTION:
    !> @brief Read a CIS type wave function.
    !> @details
    !! Read a CIS type wave function from a file. See the CISWFWrite subroutine for the format
    !! of the file.
    !----------------------------------------------------------------------------------------------
    subroutine ciswfread(infile, wf)
        character(len=*), intent(in) :: infile !< Input file.
        type(ciswf), intent(out) :: wf !< Wave function.
        integer :: inunit !< Unit.
        integer :: st !< State iterator.
        integer :: s !< Spin iterator.
        integer :: o !< Occupied orbital iterator.

        open(newunit=inunit, file=infile)
        read(inunit, *) wf%n, wf%rhf
        if (.not. allocated(wf%e)) allocate(wf%e(wf%n))
        if (.not. allocated(wf%no)) allocate(wf%no(wf%rhf))
        if (.not. allocated(wf%nv)) allocate(wf%nv(wf%rhf))
        if (.not. allocated(wf%c)) allocate(wf%c(wf%rhf, wf%n))
        do s = 1, wf%rhf
            read(inunit, *) wf%no(s), wf%nv(s)
            do st = 1, wf%n
                if (.not. allocated(wf%c(s, st)%c)) allocate(wf%c(s, st)%c(wf%no(s), wf%nv(s)))
            end do
        end do
        do st = 1, wf%n
            read(inunit, *) wf%e(st)
            do s = 1, wf%rhf
                do o = 1, wf%no(s)
                    read(inunit, *) wf%c(s, st)%c(o, :) 
                end do
            end do
        end do
        close(inunit)
    end subroutine ciswfread


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: CISWFRead_array
    !
    ! DESCRIPTION:
    !> @brief Read a CIS type wave function.
    !> @details
    !! Read a CIS type wave function from a file. See the CISWFWrite subroutine for the format
    !! of the file.
    !----------------------------------------------------------------------------------------------
    subroutine ciswfread_array(infile, rhf, no, nv, ns, wf, en)
        character(len=*), intent(in) :: infile !< Input file.
        integer, intent(out) :: rhf !< Number of spin functions.
        integer, allocatable, intent(out) :: no(:) !< Number of occupied orbitals. Dimension: rhf.
        integer, allocatable, intent(out) :: nv(:) !< Number of virtual orbitals. Dimension: rhf.
        integer, intent(out) :: ns !< Number of states.
        type(rmat), allocatable, intent(out) :: wf(:) !< Wave function coefficients for each spin.
                                                      !! Dimension: rhf
                                                      !! Dimensions of wf(s)%c: no(s)*nv(s), ns
        real(dp), allocatable :: en(:)
        integer :: inunit !< Unit.
        integer :: st !< State iterator.
        integer :: s !< Spin iterator.
        integer :: o !< Occupied orbital iterator.

        open(newunit=inunit, file=infile)
        read(inunit, *) ns, rhf
        if (.not. allocated(en)) allocate(en(ns))
        if (.not. allocated(no)) allocate(no(rhf))
        if (.not. allocated(nv)) allocate(nv(rhf))
        if (.not. allocated(wf)) allocate(wf(rhf))
        do s = 1, rhf
            read(inunit, *) no(s), nv(s)
        end do
        do st = 1, ns
            read(inunit, *) en(st)
            do s = 1, rhf
                if (.not. allocated(wf(s)%c)) allocate(wf(s)%c(no(s)*nv(s), ns))
                do o = 1, no(s)
                    read(inunit, *) wf(s)%c((o-1)*nv(s)+1:o*nv(s), st) 
                end do
            end do
        end do
        close(inunit)
    end subroutine ciswfread_array

end module ciswftype
