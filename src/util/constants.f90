!--------------------------------------------------------------------------------------------------
! MODULE: constants
!
! DESCRIPTION:
!> @brief Definitions of number types and parameters for unit conversions.
!--------------------------------------------------------------------------------------------------
MODULE constants
    use, intrinsic :: iso_fortran_env, only : stderr => error_unit, &
                                              stdout => output_unit, &
                                              stdin => input_unit

    !----------------------------------------------------------------------------------------------
    ! Definitions of the single/double/quadruple real number type.
    !----------------------------------------------------------------------------------------------
    integer, parameter :: sp = kind(1.0)                               !< Single precision real
    integer, parameter :: dp = selected_real_kind(2*precision(1.0_sp)) !< Double precision real
    integer, parameter :: qp = selected_real_kind(2*precision(1.0_dp)) !< Quadruple precision real
    real(dp), parameter :: tinydp = 0.000000000000001_dp

    integer, parameter :: ki4 = selected_int_kind(9)           !< Single precision integer
    integer, parameter :: ki8 = selected_int_kind(18)          !< Double precision integer
    integer, parameter :: kc4 = sp                             !< Single precision complex
    integer, parameter :: kc8 = dp                             !< Double precision complex

    !----------------------------------------------------------------------------------------------
    ! Unit conversions.
    !----------------------------------------------------------------------------------------------
    real(dp), parameter :: Da_me = 1822.88849_dp !< Dalton (unified atomic mass) to electron rest mass.
    real(dp), parameter :: a0_A = 0.52917721067_dp !< Bohr radius to Angstrom.
    real(dp), parameter :: aut_fs = 0.02418884326509_dp !< Atomic unit of time to femtosecond.
    real(dp), parameter :: aut_ps = 0.00002418884326509_dp !< Atomic unit of time to picosecond.
    real(dp), parameter :: eh_k = 315775.13_dp !< Hartree to Kelvin.
    real(dp), parameter :: eh_cm1 = 219474.6313702_dp !< Hartree to reciprocal centimeter.
    real(dp), parameter :: eh_eV = 27.21138602_dp !< Hartree to electronvolt.
    real(dp), parameter :: kcalmol_eh = 0.001593601_dp !< kcal/mol to Hartree.


    !----------------------------------------------------------------------------------------------
    ! TYPE: IVec
    !> @brief Array of integer vectors of varying dimensions.
    !----------------------------------------------------------------------------------------------
    type ivec 
       integer, allocatable :: c(:) !< Coefficents of the vectors.
    end type ivec


    !----------------------------------------------------------------------------------------------
    ! TYPE: IMat
    !> @brief Array of integer matrices of varying dimensions.
    !----------------------------------------------------------------------------------------------
    type imat
        integer, allocatable :: c(:, :) !< Coefficients of the matrices.
    end type imat


    !----------------------------------------------------------------------------------------------
    ! TYPE: RVec
    !> @brief Array of real vectors of varying dimensions.
    !----------------------------------------------------------------------------------------------
    type rvec 
       real(dp), allocatable :: c(:) !< Coefficents of the vectors.
    end type rvec


    !----------------------------------------------------------------------------------------------
    ! TYPE: RMat
    !> @brief Array of real matrices of varying dimensions.
    !----------------------------------------------------------------------------------------------
    type rmat
        real(dp), allocatable :: c(:, :) !< Coefficients of the matrices.
    end type rmat


    !----------------------------------------------------------------------------------------------
    ! TYPE: LVec
    !> @brief Array of logical vectors of varying dimensions.
    !----------------------------------------------------------------------------------------------
    type lvec
       logical, allocatable :: l(:)
    end type lvec


    !----------------------------------------------------------------------------------------------
    ! TYPE: LMat
    !> @brief Array of logical matrices of varying dimensions.
    !----------------------------------------------------------------------------------------------
    type lmat
       logical, allocatable :: l(:, :)
    end type lmat


 
END MODULE constants
