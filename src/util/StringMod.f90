!--------------------------------------------------------------------------------------------------
! MODULE: StringMod
!
! DESCRIPTION: 
!> Module containing helper subroutines for reading and parsing lines of text from input files.
!
!--------------------------------------------------------------------------------------------------
MODULE StringMod
    use, intrinsic :: iso_fortran_env, only : stderr => error_unit, &
                                              stdout => output_unit, &
                                              eof => iostat_end
    implicit none
    
    private
    public :: Parser
    public :: Line
    public :: ToUpper
    public :: ToLower
    public :: ReadIndexList
    public :: StrInFile
    
    !----------------------------------------------------------------------------------------------
    ! TYPE: Parser
    !
    ! DESCRIPTION:
    !> @brief 
    !! Data type for reading and parsing lines from input.
    !> @details 
    !! The ReadLine procedure reads a string of arbitrary length from an open unit and stores it
    !! to the Str variable. The Parse procedure separates this string into sections separated by
    !! delimiters passed to the procedure. The sections of the string (with the delimiters removed)
    !! are stored in the Args array. The number of sections found in the string is stored in the
    !! NArg variable.
    !! Example use:
    !!
    !!     type(parser) :: line
    !!     open(newunit=inunit, file=infile)
    !!     call line%readline(inunit)
    !!     call line%parse(' ')
    !!     do i = 1, line%nargs
    !!         do something with line%args(i) ...
    !!     end do
    !!
    !----------------------------------------------------------------------------------------------
    type Parser
        character(len=150), allocatable :: Str
        character(len=80) :: Args(50)
        integer :: NArg
    contains
       procedure :: ReadLine => parser_readline 
       procedure :: Parse => parser_parse
    end type Parser

    type(Parser) :: Line

    CONTAINS


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadLine
    !
    ! DESCRIPTION:
    !> @brief 
    !! Reads current line from an open unit.
    !> @details 
    !! Can handle lines of arbitrary length. Empty lines are skipped. If given the optional
    !! character argument 'comment', anything after the first appearance of this character is
    !! ignored. If the comment character appears at the beginning of the line, the entire line is
    !! skipped.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE Parser_ReadLine(Self, inunit, comment, err)
        class(Parser) :: Self
        integer, intent(in) :: inunit !< Unit from which the line is read.
        character(len=1), intent(in), optional :: comment !< Comment flag.
        integer, intent(out), optional :: err
      
        integer :: sz
        integer :: io
        integer :: ipos
        character(len=256) :: buffer
      
100     Self%Str = ''
        DO 
            read(inunit, "(a)", advance='no', iostat=io, size=sz) buffer
            if (io == eof .or. io > 0) THEN
                if (.not. present(err)) STOP 'Error in StringMod module, ReadLine subroutine.'
                err = io
                return
            end if
            Self%Str = Self%Str // buffer(:sz)
            if (io < 0) then
                if (present(err)) err = 0
                exit
            end if
        END DO
      
        Self%Str = trim(adjustl(Self%Str))

        IF (len_trim(Self%Str) == 0) GoTo 100
        IF (present(comment)) THEN
            ipos = index(Self%Str,comment)
            if (ipos == 1) GoTo 100
            if (ipos /= 0) Self%Str = Self%Str(:ipos-1)
        END IF
       
    END SUBROUTINE Parser_ReadLine


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Parse
    !
    ! DESCRIPTION:
    !> @brief 
    !! Parse a string into an array of arguments args(1), ..., args(nargs).
    !> @details 
    !! The string is separated based on the delimiters contained in string 'delims'. Consecutive
    !! delimiters are treated as one. Whitespace at the beginning and end of each argument is 
    !! removed.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE Parser_Parse(Self, delims)
        class(Parser) :: Self
        character(len=*), intent(in) :: delims !< String containing the possible delimiters between arguments.
      
        character(len=:), allocatable :: tempstr
        character(len=:), allocatable :: tempout
      
      
        tempstr = Self%Str
        call compact(tempstr)
        Self%NArg = 0
        Self%Args = ''
      
        DO
            IF (len_trim(tempstr) == 0) EXIT
            Self%NArg = Self%NArg + 1
            CALL split(tempstr, delims, tempout)
            Self%Args(Self%NArg) = tempout
        END DO
 
    END SUBROUTINE Parser_Parse
 
 
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadIndexList
    !
    ! DESCRIPTION:
    !> @brief 
    !! Read a list of integers from a string.
    !> @details 
    !! The integers can be given as a comma separated list. Members of the list can be single
    !! integers or ranges separated by a hyphen. All whitespace is ignored.
    !
    !> @todo Output sorted list.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE ReadIndexList(str, indexlist)
        character(len=*), intent(in) :: str !< Input string.
        integer, allocatable, intent(out) :: indexlist(:) !< Final integer list.
       
        character(len=:), allocatable :: tempstr
        character(len=:), allocatable :: crange
        character(len=:), allocatable :: part1
        integer :: i
        integer :: ntot
        integer :: range0
        integer :: range1
        integer :: templist(20000)
       
        ntot = 0
        tempstr = str
        call rmWhite(tempstr)
       
        DO
            IF (len_trim(tempstr) == 0) EXIT
            CALL split(tempstr, ',', crange)
            CALL split(crange, '-', part1)
            read(part1, *) range0
            range1 = range0
            IF (crange /= '') read(crange, *) range1
            IF ((range1 - range0) < 0) STOP 'Error. Negative range ReadIndexList.'
            DO i = 0, range1 - range0
                ntot = ntot + 1
                templist(ntot) = range0 + i
            END DO
        END DO
       
        IF (allocated(indexlist)) deallocate(indexlist)
        allocate(indexlist(ntot))
        indexlist = templist(1:ntot)
 
    END SUBROUTINE ReadIndexList
 
 
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: rmWhiteSpace
    !
    ! DESCRIPTION:
    !> @brief 
    !! Remove all spaces, tabs and control characters from a string.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE rmWhite(str)
       character(len=:), intent(inout), allocatable :: str
 
       character(len=:), allocatable :: outstr
       character(len=1) :: ch
       integer :: i
       integer :: ich
       str = adjustl(str)
       outstr = ''
 
       DO i = 1, len_trim(str)
          ch = str(i:i)
          ich = iachar(ch)
          SELECT CASE (ich)
             case(33:)
                outstr = outstr // ch
          END SELECT
       END DO
 
       str = adjustl(outstr)
 
    END SUBROUTINE rmWhite
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: compact
    !
    ! DESCRIPTION:
    !> @brief 
    !! Convert multiple spaces and tabs to single spaces and remove control characters from a 
    !! string.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE compact(str)
        character(len=:), intent(inout), allocatable :: str
      
        character(len=:), allocatable :: outstr
        character(len=1) :: ch
        integer :: i
        integer :: isp
        integer :: ich
      
        str = adjustl(str)
        outstr = ''
        isp = 0
        
        DO i = 1, len_trim(str)
            ch = str(i:i)
            ich = iachar(ch)
            SELECT CASE (ich)
                case(9,32)
                    IF (isp == 0) outstr = outstr // ' '
                    isp = 1
                case(33:)
                    outstr = outstr // ch
                    isp = 0
            END SELECT
        END DO
      
        str = adjustl(outstr)
        
    END SUBROUTINE compact
 
    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: split
    !
    ! DESCRIPTION:
    !> @brief 
    !! Search input string for first occurence of any character from delimiters string and split
    !! it at the position.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE split(str, delims, before)
        character(len=:), intent(inout), allocatable :: str !< Input string. Second part of output string.
        character(len=*), intent(in) :: delims !< Characters at which the string is split.
        character(len=:), intent(out), allocatable :: before !< First part of output string.
        
        character :: ch
        integer :: i
        integer :: ipos
        logical :: found
        character(len=:), allocatable :: tmpstr
        
        found = .false.
        str = adjustl(str)
        before = ''
        DO i = 1, len_trim(str)
            ch = str(i:i)
            ipos = index(delims, ch)
            IF (ipos == 0) THEN
                IF (found) THEN
                    ipos = len(str)
                    tmpstr = str(i:len(str))
                    str = tmpstr
                    EXIT
                END IF
                before = before // ch
            else
                IF (before == '') CYCLE
                IF (i == len_trim(str)) THEN
                    found = .false.
                    EXIT
                END IF
                found = .true.
            END IF
        END DO
        
        IF (.not. found) str = ''
        
    END SUBROUTINE split

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ToUpper
    !
    ! DESCRIPTION:
    !> @brief 
    !! Convert all letters in a string to upper case. (Only works with ASCII characters).
    !----------------------------------------------------------------------------------------------
    function ToUpper(strIn) result(strOut)
        character(len=*), intent(in) :: strIn
        character(len=:), allocatable :: strOut
        integer :: i
        integer :: j
        integer :: offset
  
        offset=iachar('A')-iachar('a')
        strOut = strIn
        do i = 1, len(strIn)
            j = iachar(strIn(i:i))
            if (j>= iachar("a") .and. j<=iachar("z") ) then
                strOut(i:i) = achar(iachar(strIn(i:i)) + offset)
            else
                strOut(i:i) = strIn(i:i)
            end if
        end do
    end function ToUpper

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ToLower
    !
    ! DESCRIPTION:
    !> @brief 
    !! Convert all letters in a string to lower case. (Only works with ASCII characters).
    !----------------------------------------------------------------------------------------------
    function ToLower(strIn) result(strOut)
        character(len=*), intent(in) :: strIn
        character(len=:), allocatable :: strOut
        integer :: i
        integer :: j
        integer :: offset
 
        offset=iachar('A')-iachar('a')
        strOut = strIn
        do i = 1, len(strIn)
            j = iachar(strIn(i:i))
            if (j>= iachar("A") .and. j<=iachar("Z") ) then
                strOut(i:i) = achar(iachar(strIn(i:i)) - offset)
            else
                strOut(i:i) = strIn(i:i)
            end if
        end do
    end function ToLower

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: StrInFile
    !
    ! DESCRIPTION:
    !> @brief 
    !! Return .true. if string is present anywhere in file. Return .false. otherwise.
    !----------------------------------------------------------------------------------------------
    function strinfile(fname, str) result(flag)
        character(len=*), intent(in) :: fname
        character(len=*), intent(in) :: str
        logical :: flag

        logical :: check
        integer :: iunit
        type(parser) :: tmpline
        integer :: ios

        flag = .false.
        inquire(file=fname, exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, TurboReadFNs subroutine.'
            write(stderr,*) ' Control file (', fname,') not found.'
            stop
        end if

        open(newunit=iunit, file=fname, action='read')
        do
             call tmpline%readline(iunit, err=ios)
            if (ios /= 0) exit
            if (index(tmpline%str, str) == 0) cycle
            flag = .true.
            exit
        end do
        close(iunit)
    end function strinfile



END MODULE StringMod

