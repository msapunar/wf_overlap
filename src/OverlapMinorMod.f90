!/--------------------------------------------------------------------------------------------------
! MODULE: CISOverlapMod
!
! DESCRIPTION: 
!--------------------------------------------------------------------------------------------------
module cisoverlapmod

    ! Import variables
    use constants
    ! Import subroutines
    use mathmod
    !$ use omp_lib

    implicit none
    
    private
    public :: cis_olap_full

contains


    subroutine cis_olap_full(csc, no, nv1, nv2, wf1, wf2, omat)
        real(dp), intent(in) :: csc(:, :, :) ! MO overlap matrix.
        integer, intent(in) :: no(:) !< Number of occupied orbitals.
        integer, intent(in) :: nv1(:) !< Number of virtual orbitals (bra).
        integer, intent(in) :: nv2(:) !< Number of virtual orbitals (ket).
        type(rmat), intent(inout) :: wf1(:) !< Ket wave function coefficients.
        type(rmat), intent(inout) :: wf2(:) !< Bra wave function coefficients.
        real(dp), allocatable, intent(out) :: omat(:, :) !< Overlap matrix.
        real(dp), allocatable :: rr(:)
        real(dp), allocatable :: rs(:, :)
        real(dp), allocatable :: sr(:, :)
        real(dp), allocatable :: ss(:, :, :)
        integer :: rhf, rhf1, rhf2, ns1, ns2, s1, s2, s
        integer :: st1, st2

        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: l1cminor(:, :)
        real(dp), allocatable :: l1rminor(:, :)
#if !defined(OPENBLAS) && !defined(MKL)
        real(dp), allocatable :: l2minor(:, :, :, :)
#endif
        ! Timing variables
        real(dp) :: start, finish
	real(dp) :: starttot, finishtot, total
	real(dp) :: wfbt, rrat, l1minort, l2minort, rsblkt, srblkt, ssblkt, omatt 

        ! External routines
#ifdef OPENBLAS 
        external :: ssblock
        integer, external :: openblas_get_num_threads
#elif MKL
        external :: ssblock
        integer, external :: mkl_get_max_threads
#endif
        integer :: num_threads

        rhf = size(csc, 3)
        rhf1 = size(wf1)
        rhf2 = size(wf2)
        ns1 = size(wf1(1)%c, 2)
        ns2 = size(wf2(1)%c, 2)
        allocate(rr(rhf))
        allocate(rs(ns2, rhf))
        allocate(sr(ns1, rhf))
        allocate(ss(ns1, ns2, rhf))
        if (.not. allocated(omat)) allocate(omat(ns1 + 1, ns2 + 1))

        starttot = omp_get_wtime()
        if (rhf == 2) then
            ! Renormalize wave functions if they have different number of spin functions:
            if (rhf1 == 1) wf1(1)%c = wf1(1)%c / sqrt(2.0_dp)
            if (rhf2 == 1) wf2(1)%c = wf2(1)%c / sqrt(2.0_dp)
        end if

        do s = 1, rhf
            s1 = min(s, rhf1)
            s2 = min(s, rhf2)
            
            ! Allocate work arrays:
#if !defined(OPENBLAS) && !defined(MKL)
            allocate(l2minor(no(s),no(s),no(s),no(s)))
#endif
            allocate(l1cminor(no(s), no(s)))
            allocate(l1rminor(no(s), no(s)))
            allocate(ref(no(s), no(s)))

            ! Set number of OpenBLAS / MKL threads to 1
#ifdef OPENBLAS
            num_threads = openblas_get_num_threads()
            call openblas_set_num_threads(1)
#elif MKL
            num_threads = mkl_get_max_threads()
            call mkl_set_num_threads(1)
#endif
            ! Calculate minors:
            ref = csc(1:no(s), 1:no(s), s)

!            print '("Computing l2minors...")'
            start = omp_get_wtime()
#if !defined(OPENBLAS) && !defined(MKL)
            call getlvl2minors(no(s), ref, l2minor)
#endif
            finish = omp_get_wtime()
            l2minort = finish - start
            
!            print '("Computing l1minors...")'
            start = omp_get_wtime()
            call getlvl1minors(no(s), ref, l1rminor, 'r')
!                write(*,*) 'Computing l1c minor'
            call getlvl1minors(no(s), ref, l1cminor, 'c')
            finish = omp_get_wtime()
            l1minort = finish -start

!            print '("Computing rrblock...")'
            ! Calculate blocks:
            start = omp_get_wtime()
            rr(s) = det(ref)
            finish = omp_get_wtime()
            rrat = finish - start
            
!            print '("Computing rsblock...")'
            start = omp_get_wtime()
            call rsblock(csc(:, :, s), no(s), nv2(s2), ns2, wf2(s2)%c, l1cminor, rs(:, s))
            finish = omp_get_wtime()
            rsblkt = finish - start
        
!            print '("Computing srblock...")'
            start = omp_get_wtime()
            call srblock(csc(:, :, s), no(s), nv1(s1), ns1, wf1(s1)%c, l1rminor, sr(:, s))
            finish = omp_get_wtime()
            srblkt = finish - start

            ! Set openblas/mkl num threads to max number
#ifdef OPENBLAS
            call openblas_set_num_threads(num_threads)
#elif MKL
            call mkl_set_num_threads(num_threads)
#endif

!            print '("Computing ssblock...")'
            start = omp_get_wtime()
#if defined(OPENBLAS) || defined(MKL)
            call ssblock(csc(:, :, s), no(s), nv1(s1), nv2(s2), ns1, ns2, wf1(s1)%c, wf2(s2)%c,    &
           &            l1rminor, ss(:, :, s))
#elif GPU
            call ssblock_gpu(csc(:, :, s), no(s), nv1(s1), nv2(s2), ns1, ns2, wf1(s1)%c, wf2(s2)%c,    &
           &            l1rminor, ss(:, :, s))
#else
            call ssblock(csc(:, :, s), no(s), nv1(s1), nv2(s2), ns1, ns2, wf1(s1)%c, wf2(s2)%c,    &
           &            l1rminor, l2minor, ss(:, :, s))
#endif
            finish = omp_get_wtime()
            ssblkt = finish - start
            
            ! Deallocate work arrays:
#if !defined(OPENBLAS) && !defined(MKL) && !defined(GPU)
            deallocate(l2minor)
#endif
            deallocate(l1cminor)
            deallocate(l1rminor)
            deallocate(ref)

        end do

        start = omp_get_wtime()
        if (rhf == 1) then
            ! Ground state overlap:
            omat(1, 1) = rr(1) * rr(1)
            ! Ground-excited state overlap:
            do st1 = 1, ns1
                omat(st1 + 1, 1) = rr(1) * sr(st1, 1)
            end do
            ! Excited-ground state overlap:
            do st2 = 1, ns2
                omat(1, st2 + 1) = rr(1) * rs(st2, 1)
            end do
            ! Excited-excited state overlap:
            do st1 = 1, ns1
                do st2 = 1, ns2
                    ! Calculate overlap matrix.
                    omat(st1 + 1, st2 + 1) = rr(1) * ss(st1, st2, 1) + sr(st1, 1) * rs(st2, 1)
                end do
            end do
        else
            ! Ground state overlap:
            omat(1, 1) = rr(1) * rr(2)
            ! Ground-excited state overlap:
            do st1 = 1, ns1
                omat(st1 + 1, 1) = rr(1) * sr(st1, 2) + rr(2) * sr(st1, 1)
            end do
            ! Excited-ground state overlap:
            do st2 = 1, ns2
                omat(1, st2 + 1) = rr(2) * rs(st2, 1) + rr(1) * rs(st2, 2)
            end do
            ! Excited-excited state overlap:
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1 + 1, st2 + 1) = rr(2) * ss(st1, st2, 1) + sr(st1, 1) * rs(st2, 2)     &
                    &                      + rr(1) * ss(st1, st2, 2) + sr(st1, 2) * rs(st2, 1)
                end do
            end do
        end if
        finish = omp_get_wtime()
        omatt = finish - start

        finishtot = omp_get_wtime()
        total = finishtot - starttot

        print '("Execution time:")'
        write(*,101) 'l2minors: ', l2minort, 'sec'
        write(*,101) 'l1minors: ', l1minort, 'sec'
        write(*,101) 'rra: ', rrat,'sec'
        write(*,101) 'rsblock: ', rsblkt, 'sec'
        write(*,101) 'srblock: ', srblkt, 'sec'
        write(*,101) 'ssblock: ', ssblkt, 'sec'
        write(*,101) 'omat: ', omatt, 'sec'
        print '("")'
        write(*,102) 'Total execution time: ', total, 'sec'
        print '("")'

        101 FORMAT(A14, G12.5, A4)
        102 FORMAT(A22, G12.5, A4)

    end subroutine cis_olap_full

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SRBlock
    !----------------------------------------------------------------------------------------------
    subroutine srblock(csc, no, nv1, ns1, wf1, l1minor, sr)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv1 !< Number of bra virtual orbitals.
        integer, intent(in) :: ns1 !< Number of bra states.
        real(dp), intent(in) :: wf1(no*nv1, ns1) !< Bra wf coefficients.
        real(dp), intent(in) :: l1minor(no, no) !< Minors of occupied part of csc matrix.
        real(dp), intent(out) :: sr(ns1)

        integer :: o
        integer :: v
        integer :: i
        integer :: st
        real(dp) :: msum

        sr = 0.0_dp
        !$omp parallel shared (sr)
        !$omp do private(msum) schedule(dynamic) reduction(+:sr)
        do o = 1, no
            do v = 1, nv1
                msum = 0
                do i = 1, no
                    msum = msum + (-1)**(o + i) * csc(no + v, i) * l1minor(o, i)
                end do
                do st = 1, ns1
                    sr(st) = sr(st) + msum * wf1((o-1)*nv1 + v, st)
                end do
            end do
        end do
        !$omp end do
        !$omp end parallel
    end subroutine srblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RSBlock
    !----------------------------------------------------------------------------------------------
    subroutine rsblock(csc, no, nv2, ns2, wf2, l1minor, rs)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv2 !< Number of ket virtual orbitals.
        integer, intent(in) :: ns2 !< Number of ket states.
        real(dp), intent(in) :: wf2(no*nv2, ns2) !< Ket wf coefficients.
        real(dp), intent(in) :: l1minor(no, no) !< Minors of occupied part of csc matrix.
        real(dp), intent(out) :: rs(ns2)

        integer :: o
        integer :: v
        integer :: i
        integer :: st
        real(dp) :: msum

        
        rs = 0.0_dp
        !$omp parallel shared(rs)
        !$omp do private(msum) schedule(dynamic) reduction(+:rs)
        do o = 1, no
            do v = 1, nv2
                msum = 0
                do i = 1, no
                    msum = msum + (-1)**(o + i) * csc(i, no + v) * l1minor(i, o)
                end do
                do st = 1, ns2
                    rs(st) = rs(st) + msum * wf2((o-1)*nv2 + v, st)
                end do
            end do
        end do
        !$omp end do
        !$omp end parallel
    end subroutine rsblock

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: GetLvl1Minors
    ! The subroutines computes the minors of the matrix per columns or rows depending
    ! on the provided value direction = {r, c}.
    !----------------------------------------------------------------------------------------------
    subroutine getlvl1minors(n, mat, dets, direction)
        integer, intent(in) :: n
        real(dp), intent(in) :: mat(n,n)
        character, intent(in) :: direction
        real(dp), intent(out) :: dets(n,n)
        
        integer :: r
        integer :: c
        integer :: seq(n)
        integer :: cmask(n-1)
        integer :: rmask(n-1)
        real(dp) :: minor(n-1, n-1)

        do r = 1, n
            seq(r) = r
        end do
        do r = 1, n
            rmask = pack(seq, seq/=r)
            do c = 1, n
                cmask = pack(seq, seq/=c)
                if ( direction == 'r') then
                   minor = mat(rmask, cmask)
                   dets(r,c) = det(minor)
                else
                   minor = mat(cmask, rmask)
                   dets(c,r) = det(minor)
                end if
            end do
        end do
    end subroutine getlvl1minors

#if !defined(OPENBLAS) && !defined(MKL) && !defined(GPU)

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SSBlock
    !----------------------------------------------------------------------------------------------
    subroutine ssblock(csc, no, nv1, nv2, ns1, ns2, wf1, wf2, l1minor, l2minor, ss)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv1 !< Number of bra virtual orbitals.
        integer, intent(in) :: nv2 !< Number of ket virtual orbitals.
        integer, intent(in) :: ns1 !< Number of bra states.
        integer, intent(in) :: ns2 !< Number of ket states.
        real(dp), intent(in) :: wf1(no*nv1, ns1) !< Bra wf coefficients.
        real(dp), intent(in) :: wf2(no*nv2, ns2) !< Ket wf coefficients.
        real(dp), intent(in) :: l2minor(no, no, no, no) !< Level 2 minors of occupied part of csc matrix.
        real(dp), intent(in) :: l1minor(no, no) !< Level 1 minors of occupied part of csc matrix.
        real(dp), intent(out) :: ss(ns1, ns2)

        integer :: o1
        integer :: o2
        integer :: v1
        integer :: v2
        integer :: i
        integer :: j
        integer :: st1
        integer :: st2
        real(dp) :: msum
        integer :: sgnj
        integer :: sgn
        integer :: s(4)

        ss = 0.0_dp
        !$omp parallel shared(ss)
        !$omp do private(msum, sgnj, sgn, s) schedule(dynamic) reduction(+:ss)
        do o1 = 1, no
          do o2 = 1, no
            do v1 = 1, nv1
              do v2 = 1, nv2
                msum = 0
                s(1) = o1
                do i = 1, no
                  if (i > o1) then
                      s(1) = i
                  else if (i < o1) then
                      s(2) = i
                  else
                      s(2) = o1
                      cycle
                  end if
                  sgnj = 1
                  s(3) = o2
                  do j = 1, no
                      if (j > o2) then
                          s(3) = j
                      else if (j < o2) then
                          s(4) = j
                      else
                          sgnj = -1
                          s(4) = o2
                          cycle
                      end if
                      sgn = sgnj * (-1)**(o1+o2+i+j)
                      msum = msum + sgn * csc(no + v1, j) * csc(i, no + v2) * l2minor(s(1), s(2), s(3), s(4))
                  end do
                end do
                msum = msum + (-1)**(o1+o2) * l1minor(o1, o2) * csc(no + v1, no + v2)
                do st1 = 1, ns1
                  do st2 = 1, ns2
                    ss(st1, st2) = ss(st1, st2) + msum * wf1((o1-1)*nv1 + v1, st1) * wf2((o2-1)*nv2 + v2, st2)
                  end do
                end do
              end do
            end do
          end do
        end do
        !$omp end do
        !$omp end parallel
    end subroutine ssblock

#endif
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: GetLvl2Minors
    !----------------------------------------------------------------------------------------------
    subroutine getlvl2minors(n, mat, dets) bind(c)
        use iso_c_binding
        integer, intent(in) :: n
        real(dp), intent(in) :: mat(n,n)
        real(dp), intent(out) :: dets(n,n,n,n)
        
        integer :: r1
        integer :: r2
        integer :: c1
        integer :: c2
        integer :: pos(4)
        integer :: seq(n)
        integer :: cmask(n-2)
        integer :: rmask(n-2)
        real(dp) :: minor(n-2, n-2)

        if (n <= 2) then
            ! Matrices of size <= 2 don't have lvl 2 minors.
            dets = 1.0_dp
            return
        end if

        do r1 = 1, n
            seq(r1) = r1
        end do

        !$omp parallel shared (n, dets, seq)
        !$omp do private(c1, c2, r2, cmask, rmask, minor) schedule(dynamic)
        do r1 = 1, n
            do r2 = 1, r1 - 1
                rmask = pack(seq, (seq/=r1 .and. seq/=r2))
                
!                !$omp parallel shared (n, dets, seq, rmask, r1, r2)
!                !$omp do private(c2, cmask, minor) schedule(dynamic)
                do c1 = 1, n
                    do c2 = 1, c1 - 1
                        cmask = pack(seq, (seq/=c1 .and. seq/=c2))
                        minor = mat(rmask, cmask)
                        dets(r1,r2,c1,c2) = det(minor)
                    end do
                end do
!                !$omp end do
!                !$omp end parallel

            end do
        end do
        !$omp end do
        !$omp end parallel

    end subroutine getlvl2minors

!#endif

end module cisoverlapmod 
