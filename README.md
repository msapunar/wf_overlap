# Wave function overlap calculation

The module is compiled using *cmake* which can compile five different executables based on different optimisation and implementation details.

Regarding the optimisation approach, two main variants are **full** and **minor**. 

The *full* variant computes the determinants of the matrices participating in the final product on-the-fly, inside the central for-loop execution part. On the other side, the *minor* variant first pre-computes the determinants of the l2-minors of the referent matrix, from which the determinants of the matrices, required in the final product, are computed inside the for-loop execution part. This method significantly reduces the number of total FLOPS.

From the implementation point of view, two variants are **original** and **blas**.
The former implements a series of the nested for-loop depending solely on scalar products. The latter is implemented relying on the BLAS-2/3 kernels (Basic Linear Algebra Subroutines) calls. Although this methods, introduces more flops than the *original* version, it achieves better performance because of better cache utilisation.

Based on the compile options and combinations the compile executables are: 

* full.exe
* full-blas.exe
* minor.exe
* minor-blas.exe
* minor-lu.exe

The executables without a suffix denote the *original* codes. The executable *minor-lu.exe* is a particular case of *minor* and *blas* variants in which the similarity between the consecutive level-2 minors is exploited in order to further decrease the number of operations. This version employs the updated LU factorisation to re-use the partial results (determinants) from the previously computed minors.

All versions are parallelised using OpenMP to speed up the execution of the for-loops and, in *blas* variants, by exploiting the multi-threaded BLAS libraries for large matrix-vector and matrix-matrix products, computed outside of the OpenMP-parallelized loops.

## Prerequisites

- cmake
- OpenBLAS/MKL
- gnu/intel/pgi compiler


## Instalation

### NO-BLAS (original) versions

    $ cmake .
    $ make
    
The results are *full.exe* and *minor.exe*.

### BLAS versions

#### OPENBLAS

Compile the code using OpenBLAS implementation of BLAS libraries

    $ set CMAKE_PREFIX_PATH=/<install_dir_openblas>/cmake
    $ cmake -DLINALG=OPENBLAS .
    $ make

The results are *full-blas.exe*, *minor-blas.exe* and *minor-lu.exe*

#### Intel MKL

Compile the code using Intel MKL implementation of BLAS libraries

    $ cmake -DLINALG=MKL .
    $ make

The results are *full-blas.exe*, *minor-blas.exe* and *minor-lu.exe*
    
## How to run

To be done

## Testing

To be done