import sys
import os
import os.path
import subprocess
import argparse
import csv

cases = []
variants = []
code_version = {'full': 'full.exe', 'minor': 'minor-exe', 'full-blas': 'full-blas.exe', 'minor-blas': 'minor-blas.exe', 'minor-lu': 'minor-lu.exe'}
field_names = ["scale", "l2minors", "l2min*wf2", "wf1*tmp", "ss", "copy", "ssblock", "total"]

def read_config(config_name):

	with open(config_name, "r") as config:
		for line in config:
			if line.startswith("variants"):
				list_split = line.replace(',','').split(':')[1].split()
				[variants.append(x) for x in list_split]
			elif line.startswith("cases"):
 				list_split= line.replace(',','').split(':')[1].split()
				[cases.append(x) for x in list_split]

def parse_output(timings, variant, output):

	found = None 
	if variant != "full-blas" and variant != "full" and variant != "minor":
		counter = 0
		for line in output.splitlines():
			if not found and line.find("[SSBLOCK] Execution times:") != -1:
				found = True
				continue
			if found and counter < 8:
				value = float(line.split()[-2])
				sum_ss = 0
				if counter < 4:
					timings[variant][field_names[counter]] = value
				elif counter >= 4 and counter <	7:
					if counter == 4:
						timings[variant][field_names[4]] = value
					else:
						timings[variant][field_names[4]] += value
				else:
					timings[variant][field_names[counter-2]] = value
				counter += 1			
			elif found and line.find("ssblock") != -1:
				ssblock = float(line.split()[-2])
				timings[variant][field_names[6]] = ssblock
			elif found and line.find("Total execution time") != -1:
				total = float(line.split()[-2])
				timings[variant][field_names[7]] = total
	elif variant == "full-blas":
		for line in output.splitlines():
			if line.find("Construct cdet") != -1:
				timings[variant][field_names[1]] = float(line.split()[-2])
			elif line.find("Generate ss") != -1:
				timings[variant][field_names[4]] = float(line.split()[-2])
			elif line.find("ssblock:") != -1:
				timings[variant][field_names[6]] = float(line.split()[-2])
			elif line.find("Total execution time") != -1:
				timings[variant][field_names[7]] = float(line.split()[-2])
	elif variant == "minor":
		for  line in output.splittlines():
			if line.find("l2minors") != -1:
				timings[variant][field_names[1]] = float(line.split()[-2])
			elif line.find("ssblock") != -1:
				timings[variant][field_names[6]] = float(line.split()[-2])
			elif line.find("Total execution time") != -1:
				timings[variant][field_names[7]] = float(line.split()[-2])
	elif variant == "full":
		for  line in output.splittlines():
			if line.find("ssblock") != -1:
				timings[variant][field_names[6]] = float(line.split()[-2])
			elif line.find("Total execution time") != -1:
				timings[variant][field_names[7]] = float(line.split()[-2])
	else:
		print("[ERROR] Unknown program variant: " + variant)
	

def main():

	# Parse input arguments. File name for the aggregated output folder.

	parser = argparse.ArgumentParser(description='Testing program that runs a series of runs of the full/minor/blas/lu code variants on the given test cases. The test cases and code variants are given in test.config file.')
	parser.add_argument("out_folder", help='Name of the folder for the aggregated outputs')
	parser.add_argument("--c", metavar="Config file", help='Name of the configuration file. [default test.config]', default='test.config')
	args = parser.parse_args()

	output_name = args.out_folder
	configfile = args.c


	# Check if folder exists, if yes add suffix 1,2,3,...
	if os.path.exists(output_name):
		output_name = output_name + '_1'
	os.mkdir(output_name)

	# Check if configuration file exists. If not, exit
	if not os.path.exists(configfile):
		print("Configuration file: " + configfile + " does not exists! Exiting...")
		exit(-1)

	read_config(configfile)

	print("\n\nTesting configuration:")
	print("-----------------------")
	print("   Cases: " + ', '.join(cases))
	print("   Code variants: " + ', '.join(variants))


	for case in cases:
		abspath = os.path.abspath('./test/' + case)
		if not os.path.exists(abspath) :
			print('Path: ' + abspath + ' does not exists!')
		else:

			print("\nCASE: " + case)
			print("-------------------------\n")

			# File to write the timing outputs in csv format
			case_output = case + '.csv'
			case_f = open(output_name+'/'+case_output, 'wb')

			writer = csv.DictWriter(case_f, fieldnames=field_names)
			writer.writeheader()

			# Empty dictionary to collect timing values
			timings = {}

			# Execute code version
			for variant in variants:
				cmd = '../../' + code_version[variant]
				print("Running " + code_version[variant] + " on " + abspath + " ...")
				output = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=abspath).communicate()[0]
				# Print the entire output to out file named case_code_version
				out_file = output_name + '/' + case + '_' + variant
				with open(out_file, 'w') as f:
					f.write(output)

				# Sub-dictionary for each variant
				timings[variant] = {}

				# Parse output
				parse_output(timings, variant, output)

				# Store output to output file
				writer.writerow(timings[variant])

				# Per line: version scale l2minors l2minors*wf2,..., ssblock, total time.
			
			#for x in timings:
			#	print (x)
			#	for y in timings[x]:
			#		print(y, ':', timings[x][y])
			case_f.close()
	print("\n")

if __name__ == '__main__':
	sys.exit(main())
